/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author BaoBinh96
 */
public class AppContants {
    // Set applicatio type
    public static final String NATIVE_TYPE = "Native";
    public static final String HYBRID_TYPE = "Hybrid";
    public static final String ANDROID_PLATFORM = "Android";
    public static final String IOS_PLATFORM = "iOS";
    public static final String PATH_APP = "E:/Do_An_Tot_Nghiep/apk/appTest.apk";
    public static final String PATH_REPORT = System.getProperty("user.dir")+ "/Report/";
    public static final String PATH_SCREENSHORT = System.getProperty("user.dir")+ "/screenshot/";
}
