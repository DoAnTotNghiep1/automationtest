/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enties;

import java.util.List;

/**
 *
 * @author BaoBinh96
 */
public class Screen {
    private String screenID;
    private String screenName;
    private List<Component> components;

    public Screen() {
    }

    public Screen(String screenID, String screenName, List<Component> components) {
        this.screenID = screenID;
        this.screenName = screenName;
        this.components = components;
    }

    public String getScreenID() {
        return screenID;
    }

    public void setScreenID(String screenID) {
        this.screenID = screenID;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @Override
    public String toString() {
        return "Screen{" + "screenID=" + screenID + ", screenName=" + screenName + ", components=" + components + '}';
    }
    
    
}
