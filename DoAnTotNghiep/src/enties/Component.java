/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enties;

/**
 *
 * @author BaoBinh96
 */
public class Component {
    private int componentID;
    private String componentName;
    private String resourceID;
    private String xpath;

    public Component() {
    }

    public Component(int componentID, String componentName, String resourceID, String xpath) {
        this.componentID = componentID;
        this.componentName = componentName;
        this.resourceID = resourceID;
        this.xpath = xpath;
    }

    public int getComponentID() {
        return componentID;
    }

    public void setComponentID(int componentID) {
        this.componentID = componentID;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getResourceID() {
        return resourceID;
    }

    public void setResourceID(String resourceID) {
        this.resourceID = resourceID;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    @Override
    public String toString() {
        return "Component{" + "componentID=" + componentID + ", componentName=" + componentName + ", resourceID=" + resourceID + ", xpath=" + xpath + '}';
    }
    
    
}
