/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enties;

import java.util.List;

/**
 *
 * @author BaoBinh96
 */
public class App {
    private String nameApp;
    private String path;
    private List<Screen> listScreen;

    public App() {
    }

    public App(String nameApp, String path, List<Screen> listScreen) {
        this.nameApp = nameApp;
        this.path = path;
        this.listScreen = listScreen;
    }

    public String getNameApp() {
        return nameApp;
    }

    public void setNameApp(String nameApp) {
        this.nameApp = nameApp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Screen> getListScreen() {
        return listScreen;
    }

    public void setListScreen(List<Screen> listScreen) {
        this.listScreen = listScreen;
    }

    @Override
    public String toString() {
        return "App{" + "nameApp=" + nameApp + ", path=" + path + ", listScreen=" + listScreen + '}';
    }
    
    
}
