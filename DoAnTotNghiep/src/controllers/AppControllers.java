/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import common.Utils;
import enties.App;

/**
 *
 * @author BaoBinh96
 */
public class AppControllers {
    public App getApp(String path){
        // convert string json to class
        Object obj = Utils.convertJsonToClass(Utils.readAllData(path), App.class);
        if (obj instanceof App) {
            return (App) obj;
        }
        return null;
    }
    
//    public static void main(String[] args) {
//        AppControllers a = new AppControllers();
//        System.out.println(a.getApp("E:\\Do_An_Tot_Nghiep\\data.json").toString());
//    }
}
