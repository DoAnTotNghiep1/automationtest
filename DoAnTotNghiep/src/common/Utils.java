/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BaoBinh96
 */
public class Utils {

    /**
     * Convert Json to class model
     *
     * @param <T>
     * @param json
     * @param classModel
     * @return
     */
    public static <T> T convertJsonToClass(String json, Class<T> classModel) {
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(json));
            // setting Lenient true
            reader.setLenient(true);
            // get String to class
            T dto = gson.fromJson(reader, classModel);
            return dto;
        } catch (JsonSyntaxException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.WARNING, null, ex);
            return null;
        }
    }
    
    
     /**
     * Read file text UTF8
     *
     * @param path
     * @return
     */
    public static String readAllData(String path) {
        BufferedReader br = null;
        InputStreamReader fr = null;
        String sCurrentLine;
        String Content = "";

        try {
            // new object nputStreamReader
            fr = new InputStreamReader(new FileInputStream(path), "UTF-8");
            br = new BufferedReader(fr);
            // start read file
            while ((sCurrentLine = br.readLine()) != null) {
                Content += sCurrentLine + '\n';
            }
            // close file
            fr.close();
            br.close();
        } catch (Exception ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
                if (br != null) {
                    br.close();
                }

            } catch (Exception ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return Content;
    }
}
