/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;
import constants.AppContants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
/**
 *
 * @author BaoBinh96
 */
public class AppiumManager {

    public AppiumManager() {
    }
    
    public static AppiumDriver settingAppAndroid(String platform, String idDevice, String version, String deviceName, String pathApp, String applicationType){
        AppiumDriver<MobileElement> appium = null;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        //Which automation engine to use
        capabilities.setCapability("automationName", "Appium");
        //Which mobile OS platform to use
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platform);
        //Mobile OS version
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, version);
        //The kind of mobile device or emulator to use
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        //The absolute local path or remote http URL to an .ipa or .apk file
        capabilities.setCapability(MobileCapabilityType.APP, new File(pathApp));
        //How long (in seconds) Appium will wait for a new command from the client before assuming the client quit and ending the session
        capabilities.setCapability("newCommandTimeout", 60 * 60 * 24);
        //Unique device identifier of the connected physical device
        capabilities.setCapability("udid", idDevice);

        //Move directly into Webview context( if app is webview)
        if (applicationType.equalsIgnoreCase(AppContants.HYBRID_TYPE)) {
            capabilities.setCapability("autoWebview", true);
        }

        // Android
        //Enable Chromedriver�s performance logging
        capabilities.setCapability("enablePerformanceLogging", true);
        //Port used to connect to the ADB server 
        capabilities.setCapability("adbPort", 5037);
        //In a web context, use native (adb) method for taking a screenshot, rather than proxying to ChromeDriver.
        capabilities.setCapability("nativeWebScreenshot", true);
        // Kill ChromeDriver session when moving to a non-ChromeDriver webview
        capabilities.setCapability("recreateChromeDriverSessions", true);
        //Amount of time to wait for Webview context to become active, in ms. 
        capabilities.setCapability("autoWebviewTimeout", 3000);
        //Doesnt stop the process of the app under test, before starting the app using adb. If the app under test is created by another anchor app, setting this false, allows the process of the anchor app to be still alive, during the start of the test app using adb
        capabilities.setCapability("dontStopAppOnReset", true);
        //Timeout in seconds while waiting for device to become ready
        capabilities.setCapability("androidDeviceReadyTimeout", 1000);
        //Timeout in milliseconds used to wait for an apk to install to the device. Defaults to 90000
        capabilities.setCapability("androidInstallTimeout", 10000);
        //Amount of time to wait for Webview context to become active, in ms. Defaults to 2000
        capabilities.setCapability("autoWebviewTimeout", 1000);
        //Skip checking and signing of app with debug keys, will work only with UiAutomator and not with selendroid
        capabilities.setCapability("noSign", true);

        capabilities.setCapability("ignoreUnimportantViews", true);
        //Disables android watchers that watch for application not responding and application crash, this will reduce cpu usage on android device/emulator.
        capabilities.setCapability("disableAndroidWatchers", true);
        capabilities.setCapability("fullReset", true);

        try {
            appium = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        } catch (MalformedURLException ex) {
            Logger.getLogger(AppiumManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        appium.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(AppiumManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appium;
    }
    public static String captureScreenshot(AppiumDriver driver){
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        return scrFile.getAbsolutePath();
    }
    
}
