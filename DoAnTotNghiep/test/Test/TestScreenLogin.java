/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import common.ExtentManager;
import common.AppiumManager;
import constants.AppContants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author BaoBinh96
 */
public class TestScreenLogin {
    
    AppiumDriver<MobileElement> driver = null;
    ExtentReports extent = null;
    
    String btnKhamPha = "com.vttm.vietteldiscovery:id/n_btn_login";
    String edtUserName = "com.vttm.vietteldiscovery:id/edtPhone";
    String edtPass = "com.vttm.vietteldiscovery:id/edtPassword";
    String btnLogin = "com.vttm.vietteldiscovery:id/btnLogin";
    
    
    @BeforeTest
    public void beforeTest() {
        System.out.println("*********************beforeTest**************************");
        driver = AppiumManager.settingAppAndroid(AppContants.ANDROID_PLATFORM, "CB5A1Y5W0S", "6.0.1", "D6502", AppContants.PATH_APP, AppContants.NATIVE_TYPE);
        extent = ExtentManager.getInstance("ReportScreenLogin");
    }

    @Test(priority = 1) 
    public void TestClickButtonKhamPha() throws IOException {
     System.out.println("*********************TestCase1**************************");
     ExtentTest test = extent.createTest("Test Click Button Kham Pha");
     MobileElement btn = driver.findElementById(btnKhamPha);
     test.log(Status.INFO, "Get componet Button Kham Pha");
     String pathScreenshot = AppiumManager.captureScreenshot(driver);
     if(btn != null){
         test.log(Status.INFO, "Button Kham Pha hien thi");
         String text = btn.getText();
         test.log(Status.INFO, "Get nhan Button");
         if(text.equals("KHÁM PHÁ ỨNG DỤNG")){
             Assert.assertTrue(true);
             test.pass("Nhan Buton la: KHÁM PHÁ ỨNG DỤNG").addScreenCaptureFromPath(pathScreenshot);
         }else{
             Assert.assertTrue(false);
             test.fail("Nhan Buton la: "+ text).addScreenCaptureFromPath(pathScreenshot);
         }
     }else{
         test.log(Status.INFO, "Button Kham Pha khong hien thi");
         Assert.assertTrue(false);
         test.fail("Button Kham Pha khong hien thi").addScreenCaptureFromPath(pathScreenshot);
     }
     extent.flush();
    }

    
    @Test(priority = 2) 
    public void TestExistTextbox() throws InterruptedException, IOException {
     System.out.println("*********************TestCase2**************************");
     ExtentTest test = extent.createTest("Test hien thi textbox user-name");
     MobileElement btn = driver.findElementById(btnKhamPha);
     test.log(Status.INFO, "Get componet Button Kham Pha");
     btn.click();
     test.log(Status.INFO, "Click Button Kham Pha");
     Thread.sleep(10000);
     MobileElement txtUserName = driver.findElementById(edtUserName);
     test.log(Status.INFO, "Get textbox user-name");
     String pathScreenshot = AppiumManager.captureScreenshot(driver);
     if(txtUserName != null){
        Assert.assertTrue(true);
        test.pass("Textbox user-name hien thi").addScreenCaptureFromPath(pathScreenshot);
     }else{
        Assert.assertTrue(false);
        test.fail("Textbox user-name khong duoc hien thi").addScreenCaptureFromPath(pathScreenshot);
     }
     extent.flush();
    }
    
    @Test(priority = 3) 
    public void TestInputUserName() throws IOException {
     System.out.println("*********************TestCase3**************************");
     ExtentTest test = extent.createTest("Test hien thi textbox user-name");
     String username = "01234567";
     MobileElement txtUserName = driver.findElementById(edtUserName);
     test.log(Status.INFO, "Get textbox user-name");
     txtUserName.click();
     test.log(Status.INFO, "Focus textbox user-name");
     txtUserName.clear();
     test.log(Status.INFO, "Clear text user-name");
     txtUserName.sendKeys(username);
     test.log(Status.INFO, "Input "+username);
     String text = txtUserName.getText();
     test.log(Status.INFO, "Get text input");
     String pathScreenshot = AppiumManager.captureScreenshot(driver);
     if(text.equals(username)){
        Assert.assertTrue(true);
        test.pass("Input thanh cong").addScreenCaptureFromPath(pathScreenshot);
     }else{
        Assert.assertTrue(false);
        test.fail("Input textbox that bai").addScreenCaptureFromPath(pathScreenshot);
     }
    }
    
    @AfterTest
    public void afterTest() {
        System.out.println("*********************afterTest**************************");
        driver.quit();
        extent.flush();
    }
}
