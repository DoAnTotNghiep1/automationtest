/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import common.ExtentManager;
import constants.AppContants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author TuanDV
 */
public class DemoTestReport {
    ExtentReports extent;
    public DemoTestReport() {
        
    }
    
    @BeforeTest
    public void beforeTest() {
        System.out.println("**********beforeTest*********");
        extent = ExtentManager.getInstance("ReportDemo");
    }
    @Test(priority = 1) 
    public void testCase1() {
        System.out.println("**********testCase1*********");
        ExtentTest test = extent.createTest("testCase1");
         Assert.assertTrue(true);
         // log(Status, details)
        test.log(Status.INFO, "This step shows usage of log(status, details)");
        test.log(Status.PASS, "This step shows usage of log(status, details)");
        // info(details)
        test.info("This step shows usage of info(details)");
        try {
            test.pass("pass").addScreenCaptureFromPath(AppContants.PATH_SCREENSHORT+"screenshort.jpg");
        } catch (IOException ex) {
            Logger.getLogger(DemoTestReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        extent.flush();
    }
    
    @Test(priority = 2) 
    public void testCase2() {
        System.out.println("**********testCase2*********");
        ExtentTest test2 = extent.createTest("testCase2");
         Assert.assertTrue(true);
         // log(Status, details)
        test2.log(Status.INFO, "This step shows usage of log(status, details)");
        test2.log(Status.PASS, "This step shows usage of log(status, details)");
        // info(details)
        test2.info("This step shows usage of info(details)");
        try {
            test2.pass("fail").addScreenCaptureFromPath(AppContants.PATH_SCREENSHORT+"screenshort.jpg");
        } catch (IOException ex) {
            Logger.getLogger(DemoTestReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        extent.flush();
    }
        
    @AfterTest
    public void afterTest() {
        System.out.println("**********afterTest*********");
        // calling flush writes everything to the log file
        extent.flush();
    }
    
}

